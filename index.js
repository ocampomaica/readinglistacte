/* 1. Create a readingListActE folder. Inside create an index.js file.
2. Install nodemon and perform npm init command in gitbash to create a package.json file.
3. Install express and mongoose. Make sure to check these dependencies insde the package.json file.
4. Create a .gitignore file and store the node_modules in it.
5. Once done with your solution, create a repo named "readingListActE" and push your solution.
6. Save the repo link on S35-C1: Express.js: Data Persistende via Mongoos ODM */

/*
Activity Instructions:
1. Create a User schema with the following fields: firstName, lastName, username, password, email.
2. Create a Product schema with the following fields: name, description, price.
3. Create a User Model and  a Product Model.
4. Create a POST request that will access the /register route which will create a user. Test this in Postman app.
5. Create a POST request that will access the /createProduct route which will create a product. Test this in Postman app.
6. Create a GET request that will access the /users route to retrieve all users from your DB. Test this in Postman.
7. Create a GET request that will access the /products route to retrieve all products from your DB. Test this in Postman.

*/



//Express Setup
const express = require('express');

//Mongoose Setup
const mongoose = require('mongoose');

//Application Setup
const app = express();

//Port
const port = 3000;

//Mongoose Connection
mongoose.connect(`mongodb+srv://admin123:admin123@cluster0.2myj4l1.mongodb.net/readingListActE?retryWrites=true&w=majority`, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

//connection to MongoDB
let db = mongoose.connection;

db.on('error', console.error.bind(console, 'Connection'));
db.once('open', ()=> console.log('Connected to MongoDB!'));

//1. Create a User schema with the following fields: firstName, lastName, username, password, email.

const userSchema = new mongoose.Schema({
    firstName: String,
    lastName: String,
    userName: String,
    password: String,
    email: String
})

//2. Create a Product schema with the following fields: name, description, price.

const productSchema = new mongoose.Schema({
    name: String,
    description: String,
    price: Number
})

//3. Create a User Model and  a Product Model.
const User = mongoose.model('User', userSchema);

//Product Model
const Product = mongoose.model('Product', productSchema);

//middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));

//4. Create a POST request that will access the /register route which will create a user. Test this in Postman app.
app.post('/users', (req, res) => {
    // User Logic
    User.findOne({userName: req.body.userName}, (error, result) => {
        if(error){
            return res.send(error)
        }else if(result != null && result.userName == req.body.userName){
            return res.send('Duplicate user found')
        }else{
            let newUser = new User({
                firstName: req.body.firstName,
                lastName: req.body.lastName,
                userName: req.body.userName,
                password: req.body.password,
                email: req.body.email
            })
            newUser.save((error, savedUser) => {
                if(error){
                    return console.err(error)
                }else{
                    return res.status(201).send('New user created')
                }
            })
        }
    })
});

//5. Create a POST request that will access the /createProduct route which will create a product. Test this in Postman app.
app.post('/products', (req, res) => {
    // User Logic
    Product.findOne({name: req.body.name}, (error, result) => {
        if(error){
            return res.send(error)
        }else if(result != null && result.name == req.body.name){
            return res.send('Duplicate product found')
        }else{
            let newProduct = new Product({
                name: req.body.name,
                description: req.body.description,
                price: req.body.price
            })
            newProduct.save((error, savedProduct) => {
                if(error){
                    return console.err(error)
                }else{
                    return res.status(201).send('New Product Added')
                }
            })
        }
    })
});

//6. Create a GET request that will access the /users route to retrieve all users from your DB. Test this in Postman.
app.get('/users', (req, res) => {
    User.find({}, (error, result) => {
        if(error){
            return res.send(error)
        }else {
            return res.status(200).json({
                tasks: result
            })
        }
    })
});

//7. Create a GET request that will access the /products route to retrieve all products from your DB. Test this in Postman.
app.get('/products', (req, res) => {
    Product.find({}, (error, result) => {
        if(error){
            return res.send(error)
        }else{
            return res.status(200).json({
                tasks: result
            })
        }
    })
});

app.listen(port, () => console.log(`Server is running at port ${port}`))